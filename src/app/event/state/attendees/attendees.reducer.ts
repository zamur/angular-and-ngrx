import { Attendee } from 'src/app/models';
import { AttendeesActions, AttendeesActionTypes } from './attendees.actions';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { act } from '@ngrx/effects';

export interface State extends EntityState<Attendee> {
  // attendees: Attendee[]; // first implementation
  loading: boolean;
  error: any;
  filterBy: string;
}

const adapter: EntityAdapter<Attendee> = createEntityAdapter<Attendee>();

export const initialState: State = adapter.getInitialState({
  // attendees: [], // first implementation
  loading: false,
  error: null,
  filterBy: 'all'
});

export function reducer(state = initialState, action: AttendeesActions): State {
  switch (action.type) {
    case AttendeesActionTypes.LoadAttendees: {
      return adapter.removeAll({
        ...state,
        loading: true,
        error: null
      });
    }
    case AttendeesActionTypes.LoadAttendeesSuccess: {
      return adapter.addAll(action.payload, {
        ...state,
        loading: false,
        // attendees: action.payload, // first implementation
        error: null
      });
    }
    case AttendeesActionTypes.LoadAttendeesFail: {
      return adapter.removeAll({
        ...state,
        loading: false,
        error: action.payload
      });
    }
    case AttendeesActionTypes.AddAttendeeSuccess: {
      return adapter.addOne(
        action.payload,
        {
          ...state,
          error: null
        }
      );
    }
    case AttendeesActionTypes.LoadAttendeesFail: {
      return {
        ...state,
        error: action.payload
      };
    }
    case AttendeesActionTypes.FilterBy: {
      return { ...state, filterBy: action.payload };
    }
    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();
