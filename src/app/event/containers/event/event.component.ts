import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { Attendee } from 'src/app/models';
import { EventService } from '../../services/event.service';
import { State } from 'src/app/state/state';
import { StartSpinner, StopSpinner } from 'src/app/state/spinner/spinner.actions';
import { getSpinner } from 'src/app/state/spinner/spinner.selectors';
import { LoadAttendees, AddAttendee } from '../../state/attendees/attendees.actions';
import { getAttendees, getFilteredAttendees } from '../../state/attendees/attendees.selectors';
import { Router } from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {
  spinner$: Observable<boolean>;
  attendees$: Observable<Attendee[]>;

  constructor(
    private store: Store<State>,
    private eventService: EventService,
    private router: Router
  ) {}

  ngOnInit() {
    // this.getAttendees(); // first implementation
    // this.spinner$ = this.store.pipe(select(state => state.spinner.isOn)); // first implementation
    // this.spinner$ = this.store.pipe(select(getSpinner)); // first implementation
    // this.attendees$ = this.store.pipe(select(getAttendees)); // second implementation
    this.attendees$ = this.store.pipe(select(getFilteredAttendees));
    this.store.dispatch(new LoadAttendees());
  }

  // getAttendees() { // first implementation
  //   this.attendees$ = this.eventService.getAttendees();
  // }

  addAttendee(attendee: Attendee) {
    // this.store.dispatch(new StartSpinner());
    // this.eventService
    //   .addAttendee(attendee)
    //   .subscribe(() => {
    //     this.store.dispatch(new StopSpinner());
    //   });  // first implementation
    this.store.dispatch(new AddAttendee(attendee));
  }

  navigate(filterBy: string) {
    this.router.navigateByUrl(`/event?filterBy=${filterBy}`);
  }
}
